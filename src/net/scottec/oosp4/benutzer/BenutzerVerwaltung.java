package net.scottec.oosp4.benutzer;

/**
 * Interface für Benutzerverwaltungen
 * Definiert grundlegende Funktionen
 */
public interface BenutzerVerwaltung {

    /**
     * Versucht einen Benutzer in das System aufzunehmen.
     * Existiert der Benutzer bereits, wird eine Exception geworfen.
     * Ist die Aufnahme erfolgreich, terminiert die Funktion ohne Meldung.
     * @param benutzer  : Der aufzunehmende Benutzer
     * @throws BenutzerDoppeltException  : Exception, falls Benutzer bereits vorhanden
     */
    void benutzerEintragen(Benutzer benutzer) throws BenutzerDoppeltException;

    /**
     * Prüft, ob Benutzer in Datenhaltung vorhanden
     * @param benutzer  : Zu überprüfender Benutzer
     * @return  True    : Benutzer gefunden
     *          False   : Benutzer nicht gefunden
     */
    boolean benutzerOk(Benutzer benutzer);
}
