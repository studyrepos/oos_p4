package net.scottec.oosp4.benutzer;

/**
 * Exception
 * Vergleichbar mit NullPointerException.
 * Geworfen, falls Benutzer nicht in Datenhaltung gefunden.
 */
public class BenutzerNichtGefundenException extends Exception {
    public BenutzerNichtGefundenException(String message) {
        super(message);
    }
}
