package net.scottec.oosp4.benutzer;

/**
 * Exception
 * Kennzeichnet Dopplung in Datenhaltung
 */
public class BenutzerDoppeltException extends Exception {
    public BenutzerDoppeltException(String message) {
        super(message);
    }
}
