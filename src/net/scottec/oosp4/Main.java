package net.scottec.oosp4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.scottec.oosp4.benutzer.BenutzerVerwaltungAdmin;

import java.io.IOException;

public class Main extends Application {

    public static BenutzerVerwaltungAdmin benutzerVerwaltungAdmin = new BenutzerVerwaltungAdmin("user.s", false);

    @Override
    public void start(Stage primaryStage) {
        Parent index, login, register;
        try {
            index = FXMLLoader.load(getClass().getResource("gui/index/anwendung.fxml"));
            primaryStage.setTitle("Benutzerverwaltung");
            primaryStage.setScene(new Scene(index));
            primaryStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }

        Stage loginStage = new Stage();
        try {
            login = FXMLLoader.load(getClass().getResource("gui/login/login.fxml"));
            loginStage.setTitle("Benutzerverwaltung");
            loginStage.setScene(new Scene(login));
            loginStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }

        Stage registerStage = new Stage();
        try {
            register = FXMLLoader.load(getClass().getResource("gui/register/anmeldung.fxml"));
            registerStage.setTitle("Benutzerverwaltung");
            registerStage.setScene(new Scene(register));
            registerStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
