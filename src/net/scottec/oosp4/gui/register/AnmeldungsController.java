package net.scottec.oosp4.gui.register;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.scottec.oosp4.Main;
import net.scottec.oosp4.benutzer.Benutzer;
import net.scottec.oosp4.benutzer.BenutzerDoppeltException;

public class AnmeldungsController {

    @FXML
    TextField txtUserId;

    @FXML
    PasswordField txtPassword;

    @FXML
    PasswordField txtPasswordRepeat;

    @FXML
    Label lbError;

    @FXML
    Button btSubmit;

    @FXML
    public void onSubmitClick(Event event) {
        System.out.print("[AnmeldungsController] Ausführen: ");
        try {
            if(txtPassword.getText().equals(txtPasswordRepeat.getText())) {
                lbError.setText("");
                Benutzer benutzer = new Benutzer(txtUserId.getText(), txtPassword.getText().toCharArray());
                Main.benutzerVerwaltungAdmin.benutzerEintragen(benutzer);
                System.out.println("Benutzer eingetragen: " + benutzer.toString());
                ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
            }
            else {
                System.out.println("Error");
                lbError.setText("Passwort != Wiederholung");
            }
        }
        catch (BenutzerDoppeltException exp) {
            System.out.println(exp.getMessage());
        }
    }
}
