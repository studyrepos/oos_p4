package net.scottec.oosp4.gui.login;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.scottec.oosp4.Main;
import net.scottec.oosp4.benutzer.Benutzer;
import net.scottec.oosp4.benutzer.BenutzerDoppeltException;

public class LoginController {

    private boolean neuAnmeldung = false;

    @FXML
    TextField txtUserId;

    @FXML
    PasswordField txtPassword;

    @FXML
    CheckBox cbNeuAnmeldung;

    @FXML
    Button btSubmit;

    @FXML
    public void onSubmitClick(Event event) {
        System.out.println("[LoginController] Ausführen:");
        try {
            Benutzer benutzer = new Benutzer(txtUserId.getText(), txtPassword.getText().toCharArray());
            Main.benutzerVerwaltungAdmin.benutzerEintragen(benutzer);
            System.out.println("[LoginController] Benutzer eingetragen: " + benutzer.toString());
        }
        catch (BenutzerDoppeltException exp) {
            System.out.println(exp.getMessage());
        }
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
    }

    @FXML
    public void onNeuAnmeldungToggle(Event event) {
        this.neuAnmeldung = cbNeuAnmeldung.isSelected();
        System.out.println("[LoginController] Neu-Anmeldung : " + this.neuAnmeldung) ;
    }
}
