package net.scottec.oosp4.gui.index;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class AnwendungsController {

    @FXML
    Label lbMessage;

    @FXML
    Button btCancel;

    @FXML
    public void onCancelClick(Event event) {
        System.out.println("[AnwendungsController] Event : Abbrechen");
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
    }
}
